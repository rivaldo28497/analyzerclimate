﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnalyzerClimate
{
    public class ErrorHandlingMIddleware
    {
        RequestDelegate _next;
        public ErrorHandlingMIddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            await _next(context);
            if (context.Response.StatusCode == 403)
                await context.Response.WriteAsync("Auth pizda");
            else if (context.Response.StatusCode == 404)
                await context.Response.WriteAsync("Path pizda");
        }
    }
}
