﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnalyzerClimate
{
    public class TokenMIddleware
    {
        private readonly RequestDelegate _next;
        string _pattern;

        public TokenMIddleware(RequestDelegate next, string pattern)
        {
            this._next = next;
            _pattern = pattern;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var token = context.Request.Query["token"];
            if (token != _pattern)
            {
                context.Response.StatusCode = 403;
                await context.Response.WriteAsync("Token is invalid");
            }
            else
                await _next.Invoke(context);
        }

    }

}
